import java.util.*;

public class FizzBuzz {                      

  public static String calculate(int index) {   

    List<String> result = new ArrayList<String>();

	for(int i = 1; i <= index; i++) { 
                       
      if (((i % 3) == 0) && ((i % 5) == 0)) {     
        result.add("FizzBuzz");      
        //System.out.println("FizzBuzz");    
      } else if ((i % 3) == 0) {
        result.add("Fizz"); 
      	//System.out.println("Fizz"); 
      } else if ((i % 5) == 0) {
        result.add("Buzz");  
      	//System.out.println("Buzz"); 
      } else {
        result.add(String.valueOf(i));  
      	//System.out.println(i);       
      }
                       
      //System.out.println(" "); 
      
    }
    
    
    //System.out.println();
    return result.get(index-1);
    
  }
}