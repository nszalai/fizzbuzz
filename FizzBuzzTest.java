import org.junit.*;
import java.util.*;
import static org.junit.Assert.*;

import org.approvaltests.Approvals;
import org.approvaltests.reporters.UseReporter;
import org.approvaltests.reporters.JunitReporter;

@UseReporter(JunitReporter.class)
public class FizzBuzzTest {

    @Test
    public void result() throws Exception {
        String actual = FizzBuzz.calculate(20);
        Approvals.verify(actual);
    }
}